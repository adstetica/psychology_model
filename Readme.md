Copy_of_Copy_of_Copy_of_Hotdog_or_not.ipynb	- проверка простой моделью ResNet-18 с BCE лоссом
Data_to_test_train_val.py - Разделить файлы на тестовую/обучающую/валидационную выборки
Download Dataset.ipynb -  скрипт для скачивания датасета из файла psycho_photo_dataset.csv
Transfer_Learning_Psychotypes.ipynb	- обучение модели методами переноса обучения (AlexNet/VGG/GoogleNet)
Visualize_pt_model.ipynb -  IPythonNotebook для проверки качества моделей и визуализации метрик (на основе файлов лоссов и accuracy после обучения)
cut_dataset.py - скрипт для разделения на тестовую/обучающую/валидационную выборки по минимальному количеству фотографий в классе
dataset_download.py - скрип для скачивания датасета по ссылкам
main (another copy).py - обучение Multi-Label GoogleNet модели классификации психотипов
main (copy).py - обучение Multi-Label ResNet модели классификации психотипов
psycho_photo_dataset.csv - файл со ссылками на картинки с психотипами